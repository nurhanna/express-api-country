import { DataTypes } from 'sequelize';

const GalleryModel = (sequelize) =>
  sequelize.define('gallery', {
    img_title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    img_url: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

export default GalleryModel;
