import express from 'express';
import galleryController from '../controllers/galleryController.js';
import upload from '../helper/multerHelper.js';

const galleryRouter = express.Router();

galleryRouter.get('/', galleryController.getGallery);
galleryRouter.get('/:id', galleryController.findImage);
galleryRouter.post('/', upload.single('image'), galleryController.addGallery);

export default galleryRouter;
