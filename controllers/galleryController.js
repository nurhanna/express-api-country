import { gallery } from '../database/db.js';
const galleryController = {
  getGallery: async (req, res) => {
    try {
      const allGalery = await gallery.findAll();
      res.json({
        status: 'success',
        statusCode: 200,
        message: 'Success get galery',
        data: allGalery,
      });
    } catch (err) {
      res.status(500).json({
        status: 'error',
        statusCode: 500,
        message: err,
      });
    }
  },

  findImage: async (req, res) => {
    try {
      const findImage = await gallery.findByPk(req.params.id);
      if (findImage) {
        res.json({
          status: 'success',
          statusCode: 200,
          message: 'Success get image by id',
          data: findImage,
        });
      } else {
        res.json({
          status: 'Error',
          statusCode: 404,
          message: 'Image not found',
        });
      }
    } catch (err) {
      console.log(err);
      res.status(500).json({
        status: 'error',
        statusCode: 500,
      });
    }
  },

  addGallery: async (req, res) => {
    try {
      console.log(req.protocol, req.get('host'));
      const newData = {
        img_title: req.file.filename,
        // img_url: req.file.destination,
        img_url: `${req.protocol}://${req.get('host')}/${req.file.filename}`,
      };
      const newGalery = await gallery.create(newData);
      // console.log(newGalery);
      if (newGalery) {
        res.json({
          status: 'success',
          statusCode: 200,
          message: 'Success add galery',
          data: newGalery,
        });
      }
    } catch (err) {
      res.status(500).json({
        status: 'error',
        statusCode: 500,
        message: err,
      });
    }
  },
};

export default galleryController;
